# RF server state services
Python apps to generate JSON API from Rf online server status.

## Getting started
Copy server-stats.exe and apps.ini to your Zone Server/SystemSave directory 
```ini
[APPCON]
serverDisplayPath=ServerDisplay.ini
serverStatePath=ServerState.ini
appServerPort=10079
appServerAddr=localhost
dataRefreshSeconds=2
```
every value in apps.ini is configurable 

run server-stats.exe and now you can access the API on http://localhost:10079
sample data
```json
{
    "users": {
        "usernum": "0",
        "b_num": "0",
        "c_num": "0",
        "a_num": "0"
    },
    "maps": {
        "mapnum": "31",
        "neutralb": "123",
        "neutralc": "24",
        "resources": "0",
        "neutrala": "0",
        "neutralbs1": "0",
        "neutralbs2": "0",
        "neutralcs1": "0",
        "neutralcs2": "0",
        "neutralas1": "0",
        "neutralas2": "0",
        "platform01": "0",
        "sette": "0",
        "cauldron01": "0",
        "elan": "0",
        "dungeon00": "0",
        "transport01": "0",
        "dungeon01": "0",
        "accgsd": "0",
        "bellagsd": "0",
        "coragsd": "0",
        "accgsp": "0",
        "bellagsp": "0",
        "coragsp": "0",
        "dungeon02": "0",
        "exile_land": "0",
        "mountain_beast": "0",
        "medicallab": "0",
        "cora": "0",
        "dungeon03": "0",
        "medicallab2": "0",
        "dungeon04": "0"
    }
}

```


## How to consume on live server
- Use Html jquery and ajax to get data from http://localhost:10079 
- Use PHP CURL to get data from http://localhost:10079



