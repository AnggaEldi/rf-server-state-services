import configparser
import json 
from http.server import BaseHTTPRequestHandler, HTTPServer

class configRead:
    APPCONF = 'apps.ini'

    def __init__(self):
        self.checkAppConfig()
        self.checkRfConfig()

    def get_addr(self):
        return self._addr
    
    def set_addr(self, newAddr):
        self._addr = newAddr

    def del_addr(self):
        del self._addr     

    def get_port(self):
        return self._port
    
    def set_port(self, newPort):
        self._port = newPort

    def del_port(self):
        del self._port  

    def get_ref(self):
        return self._ref
    
    def set_ref(self, newref):
        self._ref = newref

    def del_ref(self):
        del self._ref  
   
    serverAddress = property(get_addr, set_addr, del_addr)
    serverPort = property(get_port, set_port, del_port)
    refreshSeconds = property(get_ref, set_ref, del_ref)

    @staticmethod
    def checkAppConfig():
        
        try: 
            config = configparser.ConfigParser()
            config.read(configRead.APPCONF)
            #set string from config to class variable
            __port = config['APPCON']['appServerPort']
            __addr = config['APPCON']['appServerAddr']
            __displayIni = config['APPCON']['serverDisplayPath']
            __refreshSecs = config['APPCON']['dataRefreshSeconds']
            configRead.serverPort = __port
            configRead.serverAddress = __addr
            configRead.appServerDisplayPath = __displayIni
            configRead.refreshSeconds = __refreshSecs
            configRead.appConfig = {
                'address': __addr,
                'port': __port
            }
        except:
            print('Cannot read apps.ini') 

    @staticmethod
    def checkRfConfig():
        try: 
            config = configparser.ConfigParser()
            config.read(configRead.appServerDisplayPath)
            __rfMap = config['MAP']
            __rfUser = config['USER']
            configRead.rfConfig = {
                'users': dict(__rfUser),
                'maps': dict(__rfMap)
            }
        except:
            print('cannot read: '+configRead.appServerDisplayPath)

   
class webHandler(BaseHTTPRequestHandler): 
    def get_resp(self):
        return self._resp
    
    def set_resp(self, newref):
        self._resp = newref

    def del_resp(self):
        del self._resp  

    rsp = property(get_resp, set_resp, del_resp)  

    @staticmethod
    def setResponse(newResponse):
        webHandler.set_resp = newResponse

    @staticmethod
    def getResponse(self):
        return webHandler.get_resp(self)

    def do_GET(self):
        _resp = configRead().rfConfig
        self.rsp = _resp
        response = json.dumps(_resp)
        #response = json.dumps({'hello': 'moto'})
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(response.encode())


class webService:
    def get_addr(self):
        return self._addr
    
    def set_addr(self, newAddr):
        self._addr = newAddr

    def del_addr(self):
        del self._port     
    
    def get_port(self):
        return self._port
    
    def set_port(self, newPort):
        self._port = newPort

    def del_port(self):
        del self._port     

    serverAddress = property(get_addr, set_addr, del_addr)
    serverPort = property(get_port, set_port, del_port)

    @staticmethod
    def run():
        serverConf = HTTPServer((webService.serverAddress, webService.serverPort), webHandler )
        try:
            print('Server started at: http://'+ webService.serverAddress + ':'+str(webService.serverPort))
            serverConf.serve_forever()
            
        except KeyboardInterrupt:
            pass
        print('Server '+ webService.serverAddress + ':'+str(webService.serverPort)+' is stopped' )
        serverConf.server_close()


if __name__  == "__main__":
    conf = configRead()
    #set response to webhandler
    
    #set properties to webService
    webService = webService()
    webService.serverAddress = conf.serverAddress  
    webService.serverPort = int(conf.serverPort)
    
    #start the webService
    webService.run()
   
    


    
        
        
